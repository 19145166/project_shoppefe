import React from "react";
import PropTypes from "prop-types";
import { dividerClasses } from "@mui/material";
import AddressShip from "./component/AddressShip";
import ProductPayment from "./component/ListProductPayment";
import CusComplaint from "./component/CusComplaint";
import ShippingOption from "./component/ShippingOption";
import TotalPayment from "./component/TotalPayment";
import Voucher from "./component/Voucher";
import ShopeeCoin from "./component/ShopeeCoin";
import MethodPayment from "./component/MethodPayment";
import Order from "./component/Order";
import styled from "styled-components";
import Header from "../../components/Header";
import HeaderForPayment from "./component/HeaderForPayment";

PaymentPage.propTypes = {};
const MainContainer = styled.div`
  width: 1200px;
  margin: 12px auto 70px;
`;
const BottomContainer = styled.div`
  display: flex;
  background-color: #fafdff;
`;

function PaymentPage(props) {
  return (
    <div>
    <HeaderForPayment></HeaderForPayment>
    <MainContainer>
      <AddressShip></AddressShip>
      <ProductPayment></ProductPayment>
      <BottomContainer>
        <CusComplaint></CusComplaint>
        <ShippingOption></ShippingOption>        
      </BottomContainer>
      <TotalPayment></TotalPayment>
      <Voucher></Voucher>
      <ShopeeCoin></ShopeeCoin>
      <MethodPayment></MethodPayment>
      <Order></Order>
    </MainContainer>
    </div>
  );
};
export default PaymentPage;
