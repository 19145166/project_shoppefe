import React from "react";
import styled from "styled-components";
import ConfirmationNumberIcon from '@mui/icons-material/ConfirmationNumber';

const Constainer = styled.div`
    width: 1200px;
    height: 80px;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
    border-radius: 3px;
    display: flex;
    align-items: center;
    background-color: #fff;
    margin-top: 25px;
`;
const ShopeeVoucher = styled.div`
    display: flex;
    margin-left: 20px;
    font-size: 20px;
    cursor: pointer;
`;
const IconVoucher = styled.div`
    color: #ee4d2d;
    cursor: pointer;
`;
const OptionVoucher = styled.div`
    margin-left: 850px;
    font-size: 0.875rem;
    color: #05a;
    cursor: pointer;
`;

const Voucher =()=>{
    return(
        <Constainer>
            <ShopeeVoucher>
                <IconVoucher><ConfirmationNumberIcon/></IconVoucher>
                <div>Shopee Voucher</div>
            </ShopeeVoucher>
            <OptionVoucher>Chọn Voucher</OptionVoucher>
        </Constainer>
    );
};
export default Voucher;