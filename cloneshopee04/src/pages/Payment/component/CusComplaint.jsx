import React from "react";
import styled from "styled-components";

const Container = styled.div`
    display: flex;
    width: 480px;
    height: 90px;
    border: 1px dashed rgba(0,0,0,.09);
    align-items: center;
`;
const Span = styled.span`
    padding: 16px;
    font-size: 0.875;
`;
const Input = styled.input`
    display: flex;
    align-items: center;
    box-shadow: inset 0 2px 0 0 rgb(0 0 0 / 2%);
    border-radius: 2px;
    height: 40px;
    box-sizing: border-box;
    border: 1px solid rgba(0,0,0,.14);
    background-color: #fff;
    transition: border-color .3s ease-in-out,box-shadow .3s ease-in-out,background-color .3s ease-in-out;
    min-width: 345px;
`;
const CusComplaint =()=>{
    return(
        <Container>
            <Span>Lời nhắn:</Span>
            <Input type="text" placeholder="   Lưu ý cho Người bán..."></Input>
        </Container>
    );
};
export default CusComplaint;