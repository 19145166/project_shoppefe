import React from "react";
import styled from "styled-components";

const Container = styled.div`
    width: 1200px;
    height: 75px;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    background-color: #fafdff;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
    border-radius: 3px;
`;
const GrayText = styled.div`
    font-size: 0.875rem;
    color: #bbb;
    display: flex;
`;
const OrangeText = styled.div`
    color: #ee4d24;
    margin-left: 20px;
    padding-right: 20px;
    font-size: 20px;
`;

const TotalPayment =()=>{
    return(
        <Container>
            <GrayText>
                <div>Tổng số tiền</div>
                <div>(1 sản phẩm):</div>
            </GrayText>
            <OrangeText>398.000</OrangeText>
        </Container>
    );
};
export default TotalPayment;