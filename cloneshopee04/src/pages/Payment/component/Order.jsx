import React from "react";
import styled from "styled-components";

const MainContainer = styled.div`
    width: 1200px;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
    border-radius: 3px;
    position: relative;
    background-color: #fffefb;
    border-top: 1px #bbb dashed;
`;
const TopCotainer = styled.div`
    border-radius: 3px;
`;
const BotContainer = styled.div`
    border-top: 1px dashed  rgba(0,0,0,.09);
    display: flex;
    height: 75px;
    align-items: center;
    font-size: 0.875rem;
`;
const Line = styled.div`
    display: flex;
    height: 50px;
    align-items: center;
`;
const Text = styled.div`
    margin-left: 890px;
    font-size: 0.875rem;
    color: #bbb;
`;
const Price = styled.div`
    position: absolute;
    right: 25px;
    font-size: 0.875rem;
    color: #bbb;
`;
const Total = styled.div`
    position: absolute;
    right: 25px;
    color: #ee4d2d;
    font-size: 28px;
`;
const GrayText = styled.div`
    margin: 0 5px;
    color: #bbb;
`;
const Button = styled.button`
    background-color: #ee4d2d;
    color: #fff;
    width: 210px;
    height: 40px;
    font-size: 20px;
    display: flex;
    align-items: center;
    justify-content: center;   
    border: 0px;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 3%);
    border-radius: 3px;    
    text-transform: capitalize;
    outline: none;
    margin-left: 50px;
    cursor: pointer;
`;
const Space = styled.div`
    width: 50px;
`;

const Order =()=>{
    return(
        <MainContainer>
            <TopCotainer>
                <Line>
                    <Text>Tổng tiền hàng</Text>
                    <Price>357.000đ</Price>
                </Line>
                <Line>
                    <Text>Phí vận chuyển</Text>
                    <Price>41.000đ</Price>
                </Line>
                <Line>
                    <Text>Tổng cộng Voucher giảm giá</Text>
                    <Price>-0đ</Price>
                </Line>
                <Line>
                    <Text>Tổng thanh toán:</Text>
                    <Total>498.000đ</Total>
                </Line>
            </TopCotainer>
            <BotContainer>
                <Space></Space>
                <GrayText>Nhấn "Đặt hàng" đồng nghĩa với việc bạn đồng ý tuân theo</GrayText>
                <a href="">Điều khoản Shopee</a>
                <GrayText>và</GrayText>
                <a href="">Điều khoản và Điều kiện</a>
                <GrayText>của hợp đồng Bảo hiểm</GrayText>
                <Button>Đặt Hàng</Button>
            </BotContainer>
        </MainContainer>
    );
};
export default Order;