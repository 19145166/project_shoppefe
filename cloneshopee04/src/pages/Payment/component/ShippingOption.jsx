import React from "react";
import styled from "styled-components";

const Contaniner = styled.div`
    width: 720px;
    border: 1px dashed rgba(0,0,0,.09);
    height: 90px;
`;
const TopLine = styled.div`
    display: flex;
    margin-top: 16px;
`;
const GreenText = styled.div`
    width: 155px;
    font-size: 0.875rem;
`;
const Text = styled.div`
    font-size: 0.875rem;
    padding: 0 180px 0 20px;
`;
const BlueText = styled.div`
    font-size: 0.875rem;
    padding: 0 180px 0 0;
    color: #05a;
    cursor: pointer;
`;
const Price = styled.div`
    font-size: 0.875rem;
`;
const GrayText = styled.div`
    color: #bbb;
    font-size: 0.875rem;
    padding: 0 0 0 175px;
`;

const ShippingOption =()=>{
    return(
        <Contaniner>
            <TopLine>
                <GreenText>Đơn vị vận chuyển:</GreenText>
                <Text>Nhanh</Text>
                <BlueText>THAY ĐỔI</BlueText>
                <Price>41.000</Price>
            </TopLine>
            <GrayText>
                <div>Nhận hàng vào 13 Th08 - 16 Th08</div>
                <div>(Nhanh tay vào ngay "Shopee Voucher" để săn mã Miễn phí vận chuyển nhé!)</div>
            </GrayText>
        </Contaniner>
    );
}
export default ShippingOption;