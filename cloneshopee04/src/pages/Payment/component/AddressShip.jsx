import React from "react";
import styled from "styled-components";
import LocationOnIcon from '@mui/icons-material/LocationOn';

const Container = styled.div`
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 5%);
    background: #fff;
    height: 130px;
`;
const TopBorder = styled.div`
    height: 3px;
    width: 100%;
    background-position-x: -30px;
    background-size: 116px 3px;
    background-image: repeating-linear-gradient(45deg,#6fa6d6,#6fa6d6 33px,transparent 0,transparent 41px,#f18d9b 0,#f18d9b 74px,transparent 0,transparent 82px);
`;
const AddressTitle = styled.div`
    color: #ee4d2d;
    margin: 20px;
    font-size: 1.125rem;
    text-transform: capitalize;
    display: flex;
`;
const AddressDetail = styled.div`
    display: flex;
`;
const Name_Phone = styled.div`
    margin: 0 20px;
    font-weight: bold;
    text-transform: capitalize;
`;
const Location = styled.div`
    margin-right: 20px;
    word-break: break-word;
`;
const Status = styled.div`
    font-size: .875rem;
    font-weight: 500;
    color: #929292;
    margin-left: 50px;
    text-transform: capitalize;
    flex-shrink: 0;
`;
const Option = styled.div`
    margin-left: 100px;
    font-size: 0.875rem;
    color: #05a;
    cursor: pointer;
`;
const AddressShip =()=>{
    return(
        <Container>
            <TopBorder></TopBorder>
            <AddressTitle>
                <LocationOnIcon/>
                <div>Địa chỉ giao hàng</div>                
            </AddressTitle>
            <AddressDetail>
                <Name_Phone>Ngô hoàng duy (+84) 334117633</Name_Phone>
                <Location>KTX ĐHQG khu B, Phường Đông Hòa, Thành Phố Dĩ An, Bình Dương</Location>
                <Status>mặc định</Status>
                <Option>THAY ĐỔI</Option>
            </AddressDetail>
        </Container>
    );
}
export default AddressShip;