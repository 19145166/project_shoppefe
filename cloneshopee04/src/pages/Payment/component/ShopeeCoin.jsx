import React from "react";
import styled from "styled-components";
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';

const Contaniner = styled.div`
    width: 1200px;
    height: 80px;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
    border-radius: 3px;
    display: flex;
    align-items: center;
    background-color: #fff;
`;
const Coin = styled.div`
    display: flex;
    margin-left: 20px;
    font-size: 20px;
    padding-right: 800px;
    cursor: pointer;
`;
const IconCoin = styled.div`
    color: rgb(255, 166, 0);
    cursor: pointer;
`;
const GrayText = styled.div`
    color: #bbb;
    font-size: 00.875rem;
    margin-left:30px ;
    display: flex;
    line-height: 30px;
`;
const Checkbox = styled.div`
    color: #bbb;
    font-size: 00.875rem;
    margin-left:3px ;
    display: flex;
    line-height: 30px;
    cursor: pointer;
`;

const ShopeeCoin =()=>{
    return(
        <Contaniner>
            <Coin>
                <IconCoin><MonetizationOnIcon/></IconCoin>
                <div>Shopee Xu</div>
                <GrayText>Không thể sử dụng Xu</GrayText>
            </Coin>
            <Checkbox>
                <div>[0đ]</div>
                <input type="checkbox"/>
            </Checkbox>
        </Contaniner>
    );
};
export default ShopeeCoin;