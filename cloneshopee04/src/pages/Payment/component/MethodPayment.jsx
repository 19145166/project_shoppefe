import React from "react";
import styled from "styled-components";

const Constainer = styled.div`
    width: 1200px;
    height: 80px;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
    border-radius: 3px;
    display: flex;
    align-items: center;
    background-color: #fff;
    margin-top: 25px;
`;
const Method = styled.div`
    display: flex;
    margin-left: 20px;
    font-size: 20px;
`;

const Text = styled.div`
    font-size: 0.875rem;
    margin-left: 550px;
`;

const OptionPayment = styled.div`
    margin-left: 100px;
    font-size: 0.875rem;
    color: #05a;
    cursor: pointer;
`;

const MethodPayment =()=>{
    return(
        <Constainer>
            <Method>Phương thức thanh toán</Method>
            <Text>Thanh toán khi nhận hàng</Text>
            <OptionPayment>THAY ĐỔI</OptionPayment>
        </Constainer>
    );
};
export default MethodPayment;