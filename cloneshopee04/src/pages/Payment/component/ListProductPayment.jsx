import React from "react";
import styled from "styled-components";
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

const Container = styled.div`
    background: #fff;
    box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
    border-radius: 3px;
    margin-top: 12px;
    width: 1200px;
    position: relative;
    
`;
const ProductHeader = styled.div`
    height: 50px;
    padding: 24px 30px 0;
    display: flex;
    align-items: center;
    width: 1200px;
`;
const Text1 = styled.div`
    padding-left: 20px;
    font-size: 1.2rem;
`;
const Text2 = styled.div`
    color: #bbb;
    font-size: 0.875rem;
    padding:30px ;
`;
const Text3 = styled.div`
    font-size: 0.875rem;
    padding-left:20px ;
`;
const Brand = styled.div`
    height: 50px;
    padding: 0 30px;
    display: flex;
    align-items: center;
    cursor: pointer;  
`;
const Span = styled.span`
    padding: 0 3px;
    color: #bbb;
`;
const GreenText =styled.div`
    color: #00bfa5;
    font-size: 0.875rem;
    display: flex;
    cursor: pointer;
`;
const ProductStack = styled.div`
    margin: 0px 0px 0px 50px;
    display: flex;
    font-size: 0.875rem;
    align-items: center;
    
`;
const ProductImg = styled.img`
    width: 40px;
    height: 40px;
    margin-right: 3px;
    cursor: pointer;
`;
const ProductName = styled.div`
    cursor: pointer;
`;
const Category = styled.div`
    color: #bbb;
    position: absolute;
    left: 550px;
`;
const Number1 = styled. div`
    position: absolute;
    left: 767px;
`;
const Number2 = styled. div`
    position: absolute;
    left: 897px;
`;
const Number3 = styled. div`
    position: absolute;
    left: 1097px;
`;
const Space600 = styled.div`
    width: 600px;
`;
const Space100 = styled.div`
    width: 100px;
`;

const ProductPayment =()=>{
    const value = 357000
    return(
        <Container>
            <ProductHeader>
                <Text1>Sản phẩm</Text1>
                <Space600></Space600>             
                <Text2>Đơn giá</Text2>
                <Text2>Số lượng</Text2>
                <Space100></Space100>
                <Text2>Thành tiền</Text2>
            </ProductHeader>

            <Brand>
                <Text3>Levents.vn</Text3>
                <Span>|</Span>
                <GreenText>
                    <QuestionAnswerIcon fontSize="small"/>
                    Chat ngay
                </GreenText>                
            </Brand>

            <ProductStack>
                <ProductImg src="https://cf.shopee.vn/file/6dd8e783dbbdcf575d5090ae704ec240_tn"></ProductImg>
                <ProductName>Áo thun tay dài LEVENTS University Longsleeve/ Navy</ProductName>
                <Category>Loai: size 2</Category>
                <Number1>{value.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</Number1>
                <Number2>1</Number2>
                <Number3>{value.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</Number3>
            </ProductStack>
        </Container>
    );
};
export default ProductPayment;