import React from "react";
import styled from "styled-components";
import { KeyboardArrowRight } from "@material-ui/icons";

const Container = styled.footer`
  min-width: 75rem;
  border-top: 4px solid #ee4d2d;
  background-color: #fff;
  color: rgba(0, 0, 0, 0.54);
  border-top-color: #ee4d2d;
  visibility: visible;
`;

const TopFooter = styled.div`
  margin-top: 3.75rem;
  padding-bottom: 1.875rem;
  border-bottom: 1px solid rgba(0, 0, 0, 0.09);
  width: 75rem;
  margin: 0 auto;
  color: rgba(0, 0, 0, 0.54);
  visibility: visible;
`;

const TopFooterContent = styled.div`
  color: rgba(0, 0, 0, 0.54);
  visibility: visible;
`;

const FooterSection = styled.section`
  padding-bottom: 1.875rem;
  color: rgba(0, 0, 0, 0.54);
  visibility: visible;
`;

const FooterSectionHeading = styled.h1`
  margin: 0;
  line-height: 1.7;
  font-size: 0.875rem;
  font-weight: 700;
  margin-block-start: 0.83em;
  margin-block-end: 0.83em;
  display: block;
  margin-inline-start: 0px;
  margin-inline-end: 0px;
  color: rgba(0, 0, 0, 0.54);
  visibility: visible;
`;

const SpanHeading = styled.span`
  font-size: 14px;
  font-weight: 700;
  line-height: 1.7;
  font-family: Roboto, sans-serif;
  color: #000000;
`;

const FooterSectionContent = styled.p`
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.54);
  line-height: 1.4;
  margin: 0.625rem 0 0;
  display: block;
  margin-block-start: 1em;
  margin-block-end: 1em;
  margin-inline-start: 0px;
  margin-inline-end: 0px;
  visibility: visible;
`;

const SpanContent = styled.span`
  color: rgba(0, 0, 0, 0.54);
  line-height: 1.4;
  visibility: visible;
  font-size: 12px;
`;

const SpanContentFirst = styled.span`
  color: #000000;
  background-color: #ffffff;
  font-family: Roboto, sans-serif;
  font-size: 12px;
`;

const U = styled.u`
  text-decoration: underline;
  font-size: 12px;
  color: rgba(0, 0, 0, 0.54);
  line-height: 1.4;
`;

const A = styled.a`
  color: inherit;
  display: inline-block;
  text-decoration: underline;
  cursor: pointer;
`;

const SpanContentLast = styled.span`
  color: #000000;
  background-color: #ffffff;
  font-family: Roboto, sans-serif;
  font-size: 12px;
`;

const FooterSectionSecondHeading = styled.h2`
  margin: 0;
  line-height: 1.7;
  font-size: 0.875rem;
  font-weight: 700;
  margin-block-start: 0.83em;
  margin-block-end: 0.83em;
  margin-inline-start: 0px;
  margin-inline-end: 0px;
  display: block;
  color: rgba(0, 0, 0, 0.54);
`;

const SpanSecondHeading = styled.span`
  font-size: 12px;
  font-weight: 700;
  line-height: 1.7;
  font-family: Roboto, sans-serif;
  color: #000000;
`;

const SpanSecond = styled.span`
  font-size: 12px;
  color: rgba(0, 0, 0, 0.54);
  line-height: 1.4;
`;
const SpanTopSecond = styled.span`
  font-size: 12px;
  color: #000000;
  font-family: Roboto, sans-serif;
  line-height: 1.4;
`;

const TopFooterButton = styled.button`
  align-items: center;
  display: flex;
  justify-content: center;
  color: #ee4d2d;
  background: none;
  border: 0;
  outline: none;
  padding: 0;
  -webkit-appearance: button;
  cursor: pointer;
  overflow: visible;
  text-transform: none;
  font: inherit;
  margin: 0;
`;

const ButtonContent = styled.div`
  margin-right: 0.625rem;
  text-transform: capitalize;
  font-size: 0.75rem;
  color: #ee4d2d;
`;

const BottomFooter = styled.div`
  background-color: #fbfbfb;
  color: rgba(0, 0, 0, 0.54);
`;

const BottomFirst = styled.div`
  width: 75rem;
  margin: auto;
  color: rgba(0, 0, 0, 0.54);
`;

const FirstTop = styled.div`
  padding: 0.3125rem;
  margin: 0 -0.3125rem;
  width: 100%;
  align-items: flex-start;
  display: flex;
  color: rgba(0, 0, 0, 0.54);
`;

const ColumnContent = styled.div`
  width: 20%;
  box-sizing: border-box;
  width: 20%;
  box-sizing: border-box;
  color: rgba(0, 0, 0, 0.54);
`;

const ColumnTitle = styled.div`
  font-size: 0.75rem;
  font-weight: 700;
  color: rgba(0, 0, 0, 0.54);
  margin-bottom: 1.25rem;
  margin-top: 2.5rem;
  text-transform: uppercase;
`;

const ColumnItems = styled.ul`
  text-decoration: none;
  display: block;
  color: rgba(0, 0, 0, 0.54);
  list-style-type: none;
  margin: 0 0 1.5625rem;
  padding: 0;
  margin-block-start: 1em;
  margin-block-end: 1em;
  margin-inline-start: 0px;
  margin-inline-end: 0px;
`;

const ColumnItem = styled.li`
  text-transform: capitalize;
  font-size: 0.75rem;
  margin-bottom: 0.75rem;
  align-content: center;
  display: flex;
  text-align: -webkit-match-parent;
  color: rgba(0, 0, 0, 0.54);
  list-style-type: none;

  &:hover {
    color: #ee4d2d;
    cursor: pointer;
  }
`;

const ColumnPaymentImg = styled.img`
  width: 75%;
`;

const SocialLink = styled.a`
  text-decoration: none;
  color: rgba(0, 0, 0, 0.54);
  align-content: center;
  display: flex;
  background-color: transparent;
  cursor: pointer;
  text-transform: capitalize;
  text-align: -webkit-match-parent;
  list-style-type: none;
  font-size: 0.75rem;

  &:hover {
    color: #ee4d2d;
    cursor: pointer;
  }
`;

const SocialItem = styled.div`
  height: 1rem;
  width: 1rem;
  margin-bottom: 0.25rem;
  margin-right: 0.625rem;
  background-size: 2068.75% 671.875%;
  background-position: 1.5873015873015872% 28.415300546448087%;
  color: rgba(0, 0, 0, 0.54);
  background-image: url(https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/3ce17addcf90b8cd3952b8ae0ffc1299.png);
`;

const SocialImg = styled.img`
  width: 100%;
`;

const NoReferrer = styled.a`
  width: 100%;
  flex-direction: row;
  display: flex;
  text-decoration: none;
  background-color: transparent;
  color: -webkit-link;
  cursor: pointer;
`;

const QrImgBottom = styled.img`
  height: 5.25rem;
  border: 0.0625rem solid rgba(0, 0, 0, 0.09);
  width: 5.25rem;
  margin-right: 0.9375rem;
  color: -webkit-link;
  cursor: pointer;
`;

const AppsBottom = styled.div`
  height: 5.25rem;
  justify-content: space-around;
  flex-direction: column;
  display: flex;
  align-items: flex-start;
  color: -webkit-link;
  cursor: pointer;
`;

const AppBottom = styled.img`
  width: 5rem;
  border: 0;
  color: -webkit-link;
  cursor: pointer;
`;

const FirstBottom = styled.div`
  justify-content: space-between;
  color: rgba(0, 0, 0, 0.54);
  padding: 2.5rem 0;
  border-top: 1px solid rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: flex-start;
`;

const CopyRight = styled.div`
  flex-shrink: 0;
  margin-right: 25px;
  line-height: 1.125rem;
  color: rgba(0, 0, 0, 0.54);
`;

const Nation = styled.div`
  flex-wrap: wrap;
  justify-content: center;
  display: flex;
  color: rgba(0, 0, 0, 0.54);
`;

const Area = styled.div`
  flex: 1 0 auto;
  text-align: right;
  margin-left: 5.5px;
  line-height: 1.125rem;
  color: rgba(0, 0, 0, 0.54);
`;

const AreaItem = styled.div`
  border-right: 1px solid rgba(0, 0, 0, 0.2);
  padding: 0 5px;
  color: rgba(0, 0, 0, 0.54);

  &:last-child {
    border-right: none;
  }
`;

const AreaItemLink = styled.a`
  text-decoration: none;
  color: rgba(0, 0, 0, 0.54);
  line-height: 1.125rem;
  background-color: transparent;
  cursor: pointer;
`;

const BottomLast = styled.div`
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
  padding: 2.625rem 0 2.3125rem;
  background: #f5f5f5;
`;

const BottomLastContent = styled.div`
  width: 75rem;
  margin: 0 auto;
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
`;

const PrivacyPolicy = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 2.5rem;
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
`;

const PrivacyPolicyContent = styled.div`
  padding: 0 1.5625rem;
  border-right: 1px solid rgba(0, 0, 0, 0.09);
  text-transform: uppercase;
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
`;

const PrivacyPolicyLink = styled.a`
  text-decoration: none;
  display: block;
  color: rgba(0, 0, 0, 0.54);
  background-color: transparent;
  cursor: pointer;
  text-transform: uppercase;
  font-size: 0.75rem;
`;

const PrivacyPolicyTilte = styled.span`
  color: rgba(0, 0, 0, 0.54);
  cursor: pointer;
  text-transform: uppercase;
  font-size: 0.75rem;
`;

const Certification = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  margin-top: 0;
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
`;

const CertificationLink = styled.a`
  text-decoration: none;
  color: rgba(0, 0, 0, 0.8);
  margin: 0 1.25rem;
  background-color: transparent;
  cursor: pointer;
  font-size: 0.75rem;
`;

const CertificationImg = styled.img`
  width: 7.5rem;
  height: 2.8125rem;
  background-size: 551.6666666666666% 477.77777777777777%;
  background-position: 14.391143911439114% 99.41176470588235%;
`;

const Company = styled.div`
  margin-bottom: 1.5625rem;
  display: block;
  width: 100%;
  text-align: center;
  margin-top: 0.5rem;
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
`;

const ShopeeInfo = styled.div`
  display: block;
  width: 100%;
  text-align: center;
  margin-top: 0.5rem;
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.65);
`;

const Footer = () => {
  return (
    <Container>
      <TopFooter>
        <TopFooterContent>
          <FooterSection>
            <FooterSectionHeading>
              <SpanHeading>
                <b>SHOPEE - GÌ CŨNG CÓ, MUA HẾT Ở SHOPEE</b>
              </SpanHeading>
            </FooterSectionHeading>

            <FooterSectionContent>
              <SpanContent>
                <SpanContentFirst>
                  Shopee - ứng dụng mua sắm trực tuyến thú vị, tin cậy, an toàn
                  và miễn phí! Shopee là nền tảng giao dịch trực tuyến hàng đầu
                  ở Đông Nam Á, có trụ sở chính ở Singapore, đã có mặt ở khắp
                  các khu vực&nbsp;
                </SpanContentFirst>
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.sg/">Singapore</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.my/">Malaysia</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.id/">Indonesia</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.th/"> Thái Lan</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.ph/">Philippines</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.tw/">Đài Loan</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.br/">Brazil</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.mx/">México</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.co/">Colombia</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.cl/">Chile</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.pl/"> Poland</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.es/"> Spain</A>
                    </b>
                  </A>
                </U>
                ,&nbsp;
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.ar/">Argentina</A>
                    </b>
                  </A>
                </U>
                .&nbsp;
                <SpanContentLast>
                  Với sự đảm bảo của Shopee, bạn sẽ mua hàng trực tuyến an tâm
                  và nhanh chóng hơn bao giờ hết!
                </SpanContentLast>
              </SpanContent>
            </FooterSectionContent>
            <FooterSectionSecondHeading>
              <SpanSecondHeading>
                MUA SẮM VÀ BÁN HÀNG ONLINE ĐƠN GIẢN, NHANH CHÓNG VÀ AN TOÀN
              </SpanSecondHeading>
            </FooterSectionSecondHeading>
            <FooterSectionContent>
              <SpanSecond>
                <SpanTopSecond>
                  Nếu bạn đang tìm kiếm một trang web để mua và bán hàng trực
                  tuyến thì Shopee.vn là một sự lựa chọn tuyệt vời dành cho bạn.
                  Bản chất của Shopee là một social E-commerce platform - nền
                  tảng trang web <b>thương mại điện tử</b> tích hợp mạng xã hội.
                  Điều này cho phép người mua và người bán hàng dễ dàng tương
                  tác, trao đổi thông tin về sản phẩm và chương trình khuyến mãi
                  của shop. Nhờ nền tảng đó, việc mua bán trên Shopee trở nên
                  nhanh chóng và đơn giản hơn. Bạn có thể trò chuyện trực tiếp
                  với nhà bán hàng để hỏi trực tiếp về mặt hàng cần mua. Còn nếu
                  bạn muốn tìm mua những dòng sản phẩm chính hãng, uy tín,
                  &nbsp;
                </SpanTopSecond>
                <U>
                  <A>
                    <b>
                      <A href="https://shopee.vn/mall/">Shopee Mall</A>
                    </b>
                  </A>
                </U>
                <SpanTopSecond>
                  &nbsp; chính là sự lựa chọn lí tưởng dành cho bạn. Để bạn có
                  thể dễ dàng khi tìm hiểu và sử dụng sản phẩm,
                </SpanTopSecond>
                <U>
                  <A>
                    <b>
                      <A href="http://shopee.vn/blog/">
                        Shopee Blog- trang blog thông tin chính thức của Shopee
                      </A>
                    </b>
                  </A>
                </U>
                <SpanTopSecond>
                  &nbsp;- sẽ giúp bạn có thể tìm được cho mình các kiến thức về
                  xu hướng thời trang, review công nghệ, mẹo làm đẹp, tin tức
                  tiêu dùng và deal giá tốt bất ngờ.
                </SpanTopSecond>
              </SpanSecond>
            </FooterSectionContent>
          </FooterSection>
        </TopFooterContent>

        <TopFooterButton>
          <ButtonContent>Xem thêm</ButtonContent>
          <KeyboardArrowRight
            style={{
              overflow: "hidden",
              stroke: "currentColor",
              fill: "currentColor",
              width: "1.5em",
              height: "1.5em",
              color: "#ee4d2d",
              cursor: "pointer",
              textTransform: "none",
              font: "inherit",
            }}
          />
        </TopFooterButton>
      </TopFooter>
      {/* End */}
      <BottomFooter>
        <BottomFirst>
          <FirstTop>
            <ColumnContent>
              <ColumnTitle>CHĂM SÓC KHÁCH HÀNG</ColumnTitle>
              <ColumnItems>
                <ColumnItem>Trung Tâm Trợ Giúp</ColumnItem>
                <ColumnItem>Shopee Blog</ColumnItem>
                <ColumnItem>Shopee Mall</ColumnItem>
                <ColumnItem>Hướng Dẫn Mua Hàng</ColumnItem>
                <ColumnItem>Hướng Dẫn Bán Hàng</ColumnItem>
                <ColumnItem>Thanh Toán</ColumnItem>
                <ColumnItem>Shopee Xu</ColumnItem>
                <ColumnItem>Vận Chuyển</ColumnItem>
                <ColumnItem>Trả Hàng & Hoàn Tiền</ColumnItem>
                <ColumnItem>Chăm Sóc Khách Hàng</ColumnItem>
                <ColumnItem>Chính Sách Bảo Hành</ColumnItem>
              </ColumnItems>
            </ColumnContent>

            <ColumnContent>
              <ColumnTitle>VỀ SHOPEE</ColumnTitle>
              <ColumnItems>
                <ColumnItem>Giới Thiệu Về Shopee Việt Nam</ColumnItem>
                <ColumnItem>Tuyển Dụng</ColumnItem>
                <ColumnItem>Điều Khoản Shopee</ColumnItem>
                <ColumnItem>Chính Sách Bảo Mật</ColumnItem>
                <ColumnItem>Chính Hãng</ColumnItem>
                <ColumnItem>Kênh Người Bán</ColumnItem>
                <ColumnItem>Flash Sales</ColumnItem>
                <ColumnItem>Chương Trình Tiếp Thị Liên Kết Shopee</ColumnItem>
                <ColumnItem>Liên Hệ Với Truyền Thông</ColumnItem>
              </ColumnItems>
            </ColumnContent>

            <ColumnContent>
              <ColumnTitle>THANH TOÁN</ColumnTitle>
              <ColumnItems>
                <ColumnPaymentImg src="https://scontent.xx.fbcdn.net/v/t1.15752-9/294183300_3321112361543420_1270688611050970058_n.png?_nc_cat=100&ccb=1-7&_nc_sid=aee45a&_nc_ohc=zx59oPThRGkAX-QqVw7&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVKDU3q2tR69J3VYlD0DHR44hxgdH3_lZgOsMwE0lzPuPg&oe=630092AD"></ColumnPaymentImg>
              </ColumnItems>
              <ColumnTitle>ĐƠN VỊ VẬN CHUYỂN</ColumnTitle>
              <ColumnItems>
                <ColumnPaymentImg src="https://scontent.xx.fbcdn.net/v/t1.15752-9/293604590_745458040108798_8352901416749756411_n.png?_nc_cat=104&ccb=1-7&_nc_sid=aee45a&_nc_ohc=ixKLe6-S__8AX9xrFkK&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVLC-dNT2yjcQ4GRCkUjQAPHIJELtX1dlqf0ytt0udPGeA&oe=63015D39"></ColumnPaymentImg>
              </ColumnItems>
            </ColumnContent>

            <ColumnContent>
              <ColumnTitle>THEO DÕI CHÚNG TÔI TRÊN</ColumnTitle>
              <ColumnItems>
                <ColumnItem>
                  <SocialLink href="https://facebook.com/ShopeeVN">
                    <SocialItem></SocialItem>
                    Facebook
                  </SocialLink>
                </ColumnItem>
                <ColumnItem>
                  <SocialLink href="https://instagram.com/Shopee_VN">
                    <SocialItem>
                      <SocialImg src="https://scontent.xx.fbcdn.net/v/t1.15752-9/289028057_1743114452716898_4924578114297399381_n.png?stp=cp0_dst-png&_nc_cat=110&ccb=1-7&_nc_sid=aee45a&_nc_ohc=kd5_DFwISH4AX8VE8rP&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVI_yt8-mET_J4MtXP6H7HqiZP3j7Ga1YRgrOIuYTm6B7Q&oe=6302C43A"></SocialImg>
                    </SocialItem>
                    Instagram
                  </SocialLink>
                </ColumnItem>
                <ColumnItem>
                  <SocialLink href="https://linkedin.com/company/shopee">
                    <SocialItem>
                      <SocialImg src="https://scontent.xx.fbcdn.net/v/t1.15752-9/290188737_783619042830405_3658264912058075548_n.png?stp=cp0_dst-png&_nc_cat=106&ccb=1-7&_nc_sid=aee45a&_nc_ohc=3ni4DQSgcIEAX-FWyau&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVKnvkKaqfC6SF9W2dBOhnzJ2GDm1RV8A94LoQWzstEHAg&oe=63029817"></SocialImg>
                    </SocialItem>
                    Linkedln
                  </SocialLink>
                </ColumnItem>
              </ColumnItems>
            </ColumnContent>

            <ColumnContent>
              <ColumnTitle>TẢI ỨNG DỤNG SHOPEE NGAY THÔI</ColumnTitle>
              <NoReferrer>
                <QrImgBottom src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/d91264e165ed6facc6178994d5afae79.png"></QrImgBottom>
                <AppsBottom>
                  <AppBottom src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/39f189e19764dab688d3850742f13718.png"></AppBottom>
                  <AppBottom src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/f4f5426ce757aea491dce94201560583.png"></AppBottom>
                  <AppBottom src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/1ae215920a31f2fc75b00d4ee9ae8551.png"></AppBottom>
                </AppsBottom>
              </NoReferrer>
            </ColumnContent>
          </FirstTop>

          <FirstBottom>
            <CopyRight>© 2022 Shopee. Tất cả các quyền được bảo lưu.</CopyRight>
            <Nation>
              <Area>Quốc gia & Khu vực:</Area>
              <AreaItem>
                <AreaItemLink>Singapore</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Indonesia</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Đài Loan</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Thái Lan</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Malaysia</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Việt Nam</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Philippines</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Brazil</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>México</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Colombia</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Chile</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Poland</AreaItemLink>
              </AreaItem>
              <AreaItem>
                <AreaItemLink>Argentina</AreaItemLink>
              </AreaItem>
            </Nation>
          </FirstBottom>
        </BottomFirst>

        <BottomLast>
          <BottomLastContent>
            <PrivacyPolicy>
              <PrivacyPolicyContent>
                <PrivacyPolicyLink>
                  <PrivacyPolicyTilte>CHÍNH SÁCH BẢO MẬT</PrivacyPolicyTilte>
                </PrivacyPolicyLink>
              </PrivacyPolicyContent>

              <PrivacyPolicyContent>
                <PrivacyPolicyLink>
                  <PrivacyPolicyTilte>QUY CHẾ HOẠT ĐỘNG</PrivacyPolicyTilte>
                </PrivacyPolicyLink>
              </PrivacyPolicyContent>

              <PrivacyPolicyContent>
                <PrivacyPolicyLink>
                  <PrivacyPolicyTilte>CHÍNH SÁCH VẬN CHUYỂN</PrivacyPolicyTilte>
                </PrivacyPolicyLink>
              </PrivacyPolicyContent>

              <PrivacyPolicyContent>
                <PrivacyPolicyLink>
                  <PrivacyPolicyTilte>
                    CHÍNH SÁCH TRẢ HÀNG VÀ HOÀN TIỀN
                  </PrivacyPolicyTilte>
                </PrivacyPolicyLink>
              </PrivacyPolicyContent>
            </PrivacyPolicy>

            <Certification>
              <CertificationLink>
                <CertificationImg src="https://scontent.xx.fbcdn.net/v/t1.15752-9/293472295_557064859451314_7851547235170163522_n.png?_nc_cat=100&ccb=1-7&_nc_sid=aee45a&_nc_ohc=sKZ0APN6GVQAX8balwT&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVKz6Kzclyg0qTm1JscP08VWmEr2ejU8wbwtM3i-zxgTUw&oe=6302D932"></CertificationImg>
              </CertificationLink>

              <CertificationLink>
                <CertificationImg src="https://scontent.xx.fbcdn.net/v/t1.15752-9/293472295_557064859451314_7851547235170163522_n.png?_nc_cat=100&ccb=1-7&_nc_sid=aee45a&_nc_ohc=sKZ0APN6GVQAX8balwT&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVKz6Kzclyg0qTm1JscP08VWmEr2ejU8wbwtM3i-zxgTUw&oe=6302D932"></CertificationImg>
              </CertificationLink>

              <CertificationLink>
                <CertificationImg
                  src="https://scontent.xx.fbcdn.net/v/t1.15752-9/291880865_3252393121710171_2832782845300573385_n.png?stp=cp0_dst-png&_nc_cat=107&ccb=1-7&_nc_sid=aee45a&_nc_ohc=U9Oo4pc5kvwAX9f2xi7&_nc_ad=z-m&_nc_cid=0&_nc_ht=scontent.xx&oh=03_AVIz_MowI82Gncw3ga55_NlaZtDiEBIEbscsyYypxNyRxA&oe=63010FE4"
                  style={{
                    backgroundSize: "1379.1666666666667% 447.9166666666667%",
                    backgroundPosition:
                      "1.6286644951140066% 92.21556886227545%",
                    width: "3.4rem",
                    height: "3rem",
                  }}
                ></CertificationImg>
              </CertificationLink>
            </Certification>

            <Company>Công ty TNHH Shopee</Company>

            <ShopeeInfo>
              Địa chỉ: Tầng 4-5-6, Tòa nhà Capital Place, số 29 đường Liễu Giai,
              Phường Ngọc Khánh, Quận Ba Đình, Thành phố Hà Nội, Việt Nam. Tổng
              đài hỗ trợ: 19001221 - Email: cskh@hotro.shopee.vn
            </ShopeeInfo>
            <ShopeeInfo>
              Chịu Trách Nhiệm Quản Lý Nội Dung: Nguyễn Đức Trí - Điện thoại
              liên hệ: 024 73081221 (ext 4678)
            </ShopeeInfo>
            <ShopeeInfo>
              Mã số doanh nghiệp: 0106773786 do Sở Kế hoạch & Đầu tư TP Hà Nội
              cấp lần đầu ngày 10/02/2015
            </ShopeeInfo>
            <ShopeeInfo>
              © 2015 - Bản quyền thuộc về Công ty TNHH Shopee
            </ShopeeInfo>
          </BottomLastContent>
        </BottomLast>
      </BottomFooter>
    </Container>
  );
};

export default Footer;
