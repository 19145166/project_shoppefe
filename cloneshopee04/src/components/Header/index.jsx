import React from "react";
import styled from "styled-components";
import {
  HelpOutline,
  KeyboardArrowDown,
  Language,
  NotificationsNone,
  Search,
  ShoppingCartOutlined,
} from "@material-ui/icons";

const ShopeeTop = styled.header`
  position: sticky;
  top: 0;
  left: 0;
  right: 0;
  transform: translateZ(0);
  z-index: 100;
  background: linear-gradient(-180deg, #f53d2d, #f63);
  transition: transform 0.2s cubic-bezier(0.4, 0, 0.2, 1);
`;

const NavbarWrapper = styled.div`
  min-width: inherit;
  height: 2.125rem;
  z-index: 400;
  background: transparent;
  position: relative;
`;

const ContainerNavbar = styled.nav`
  width: inherit;
  max-width: 1200px;
  color: #fff;
  margin-right: auto;
  margin-left: auto;
  display: flex;
  align-items: center;
`;

const LeftNavbar = styled.div`
  display: flex;
  align-items: center;
`;

const LeftNavbarLinkLeft = styled.a`
  color: #fff;
  text-decoration: none;
  font-size: 0.8125rem;
  font-weight: 450;
  padding: 0.25rem;
  position: relative;
  overflow: visible;
  outline: 0;
  background-color: transparent;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }
`;
const LeftNavbarLinkRight = styled.a`
  color: #fff;
  text-decoration: none;
  font-size: 0.8125rem;
  font-weight: 450;
  padding: 0.25rem;
  position: relative;
  overflow: visible;
  outline: 0;
  background-color: transparent;
  margin-left: 0.625rem;
  display: flex;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }

  &::after {
    content: "";
    height: 0.9375rem;
    width: 0;
    border-left: 1px solid hsla(0, 0%, 100%, 0.22);
    border-right: 1px solid hsla(0, 0%, 100%, 0.22);
    position: absolute;
    left: -6px;
    top: calc(50% - 7px);
  }
`;

const NavbarQr = styled.div`
  margin-left: 0.625rem;
  display: flex;
  position: relative;
  overflow: visible;
  outline: 0;
  color: #fff;
`;

const ConnectNavbar = styled.div`
  margin-left: 0.625rem;
  display: flex;
  padding-right: 0;
  border: 0;
  color: #fff;
  text-decoration: none;
  font-size: 0.8125rem;
  font-weight: 450;
  padding: 0.25rem;
  position: relative;
  overflow: visible;
  outline: 0;

  &::after {
    content: "";
    height: 0.9375rem;
    width: 0;
    border-left: 1px solid hsla(0, 0%, 100%, 0.22);
    border-right: 1px solid hsla(0, 0%, 100%, 0.22);
    position: absolute;
    left: -6px;
    top: calc(50% - 7px);
  }
`;

const SocialNavbar = styled.div`
  border: 0;
  display: flex;
  padding-right: 0;
  color: #fff;
  text-decoration: none;
  font-size: 0.8125rem;
  font-weight: 500;
  padding: 0.25rem;
  position: relative;
  overflow: visible;
  outline: 0;
`;

const FacebookLink = styled.a`
  width: 16px;
  height: 16px;
  display: inline-block;
  overflow: hidden;
  text-indent: -9999px;
  text-align: left;
  margin-right: 10px;
  background-size: 487.5% 293.75%;
  background-position: 8.064516129032258% 16.129032258064516%;
  background-image: url(https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/cab134ca96b0829b591cfaff892ae62c.png);
  text-decoration: none;
  background-color: transparent;
  color: -webkit-link;
  cursor: pointer;
  font-size: 0.8125rem;
  font-weight: 300;
`;

const InstagramLink = styled.a`
  width: 16px;
  height: 16px;
  display: inline-block;
  overflow: hidden;
  text-indent: -9999px;
  text-align: left;
  margin-right: 10px;
  background-size: 487.5% 293.75%;
  background-position: 58.064516129032256% 16.129032258064516%;
  text-decoration: none;
  background-color: transparent;
  color: -webkit-link;
  cursor: pointer;
  font-size: 0.8125rem;
  font-weight: 300;
  background-image: url(https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/cab134ca96b0829b591cfaff892ae62c.png);
`;

const CenterNavbar = styled.div`
  flex: 1;
  color: #fff;
`;

const RightNavbar = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  padding: 0;
  color: #fff;
  margin: 0;
  height: 2.125rem;
`;

const NotificationNavbar = styled.li`
  cursor: pointer;
  padding: 0;
  user-select: none;
  position: relative;
  justify-content: center;
  display: flex;
  align-items: center;
  color: currentColor;
  list-style: none;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }
`;

const NotiWrapper = styled.div`
  position: relative;
  cursor: pointer;
  user-select: none;
  color: currentColor;
  list-style: none;
`;

const NotiButton = styled.div`
  cursor: pointer;
  user-select: none;
  color: currentColor;
  list-style: none;
  text-align: -webkit-match-parent;
`;

const NotiLink = styled.a`
  pointer-events: none;
  background-color: transparent;
  user-select: none;
  cursor: pointer;
  color: currentColor;
  list-style: none;
  align-items: center;
  display: flex;
  text-decoration: none;
`;

const NotiTitle = styled.span`
  margin-left: 0.3125rem;
  font-weight: 450;
  font-size: 0.8125rem;
  text-transform: capitalize;
  pointer-events: none;
  color: currentColor;
  cursor: pointer;
  user-select: none;
  list-style: none;
`;

const SupportNavbar = styled.a`
  padding-left: 0;
  cursor: pointer;
  user-select: none;
  position: relative;
  justify-content: center;
  padding: 0 0.625rem;
  display: flex;
  align-items: center;
  color: currentColor;
  text-decoration: none;
  background-color: transparent;
  list-style: none;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }
`;

const SupportIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 0.3125rem;
  margin-left: 0.5rem;
  color: currentColor;
  cursor: pointer;
  user-select: none;
  list-style: none;
`;

const SupportTitle = styled.span`
  cursor: pointer;
  user-select: none;
  list-style: none;
  display: flex;
  align-items: center;
  font-weight: 450;
  font-size: 0.8125rem;
  color: currentColor;
  text-transform: capitalize;
`;

const LanguageNavbar = styled.li`
  cursor: pointer;
  padding: 0;
  user-select: none;
  position: relative;
  justify-content: center;
  display: flex;
  align-items: center;
  color: currentColor;
  list-style: none;
`;

const LanguageWrapper = styled.div`
  position: relative;
  cursor: pointer;
  user-select: none;
  color: currentColor;
  list-style: none;
`;

const LanguageButton = styled.div`
  list-style: none;
  color: currentColor;
  user-select: none;
  cursor: pointer;
`;

const LanguageItems = styled.div`
  display: flex;
  align-items: center;
  padding: 0.4375rem 0.625rem;
  color: #fff;
  cursor: pointer;
  user-select: none;
  list-style: none;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
  }
`;

const LanguageTitle = styled.div`
  font-weight: 450;
  color: #fff;
  cursor: pointer;
  user-select: none;
  list-style: none;
  font-size: 0.8125rem;
  margin: 0 0.3125rem;
  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
  }
`;

const RegisterNavbar = styled.a`
  cursor: pointer;
  color: currentColor;
  text-transform: none;
  font-weight: 500;
  font-size: 0.8125rem;
  position: relative;
  justify-content: center;
  padding: 0 0.625rem;
  text-decoration: none;
  background-color: transparent;
  list-style: none;
  display: flex;
  align-items: center;
  user-select: none;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }
`;

const SeparatorNavbar = styled.div`
  border-right: 1px solid hsla(0, 0%, 100%, 0.4);
  height: 0.8125rem;
  list-style: none;
  color: #fff;
`;

const LoginNavbar = styled.a`
  cursor: pointer;
  color: currentColor;
  text-transform: none;
  font-weight: 500;
  font-size: 0.8125rem;
  position: relative;
  justify-content: center;
  padding: 0 0.625rem;
  text-decoration: none;
  background-color: transparent;
  list-style: none;
  display: flex;
  align-items: center;
  user-select: none;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }
`;

const ContainerWrapper = styled.div`
  min-width: inherit;
  background: transparent;
  box-shadow: 0 1px 1px 0 rgb(0 0 0 / 5%);
  box-sizing: border-box;
  z-index: 300;
`;

const ContainerWithSearh = styled.div`
  width: inherit;
  max-width: 1200px;
  margin-right: auto;
  margin-left: auto;
  box-sizing: border-box;
  display: flex;
  height: 5.3125rem;
  justify-content: space-between;
  padding: 1rem 0 0.625rem;
`;

const HeaderLogo = styled.a`
  position: relative;
  top: -0.1875rem;
  padding-right: 2.5rem;
  box-sizing: border-box;
  cursor: pointer;
  text-decoration: none;
  background-color: transparent;
  color: -webkit-link;
`;

const LogoWrapper = styled.div`
  padding: 2px;
  margin: -2px;
  border-radius: 2px;
  cursor: pointer;
  color: -webkit-link;
`;

const LogoImg = styled.img`
  width: 162px;
  height: 50px;
  display: block;
  overflow: hidden;
  color: #fff;
  fill: none;
  position: relative;
  cursor: pointer;
  fill-rule: evenodd;
  transform-origin: 0px 0px;
`;

const SearchSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  width: 840px;
  position: relative;
`;

const SearchBar = styled.div`
  width: 100%;
  box-shadow: 0 0.125rem 0.25rem rgb(0 0 0 / 9%);
  --focus-indicator-spacing: 3px;
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  height: 2.5rem;
  box-sizing: border-box;
  padding: 0.1875rem;
  border-radius: 2px;
  background: #fff;
`;

const SearchBarMain = styled.div`
  display: flex;
  flex: 1;
  --focus-indicator-spacing: 3px;
`;

const SearchInputForm = styled.form`
  background-color: #fff;
  border-color: #fff;
  display: flex;
  flex: 1;
  box-sizing: border-box;
  padding: 0 0.625rem;
  position: relative;
  margin-top: 0em;
  --focus-indicator-spacing: 3px;
`;

const SearchInput = styled.input`
  display: flex;
  flex: 1;
  align-items: center;
  outline: none;
  border: 0;
  padding: 0;
  margin: 0;
  line-height: normal;
  color: inherit;
  font: inherit;
  --focus-indicator-spacing: 3px;
`;

const SearchButton = styled.div`
  position: relative;
  overflow: visible;
  outline: 0;
  background: #fb5533;
  color: #fff;
  height: 34px;
  padding: 0 15px;
  min-width: 60px;
  max-width: 190px;
  display: inline-flex;
  text-overflow: ellipsis;
  -webkit-line-clamp: 1;
  flex-direction: column;
  font-size: 14px;
  box-sizing: border-box;
  box-shadow: 0 1px 1px 0 rgb(0 0 0 / 9%);
  border-radius: 2px;
  border: 0;
  align-items: center;
  justify-content: center;
  text-transform: capitalize;
  cursor: pointer;

  &:hover {
    background: #fb6445;
  }
`;

const SearchBottom = styled.div`
  width: 100%;
  display: flex;
  flex: 1;
`;

const SearchBottomItems = styled.div`
  flex-wrap: wrap;
  position: relative;
  overflow-y: clip;
  margin-top: 0.1875rem;
  height: 1.5rem;
  font-size: 0.75rem;
  font-weight: 450;
  line-height: 1.5rem;
  width: 100%;
  display: flex;
`;

const SearchItemLink = styled.a`
  margin-left: 10px;
  text-decoration: none;
  position: relative;
  overflow: visible;
  outline: 0;
  white-space: nowrap;
  display: block;
  color: hsla(0, 0%, 100%, 0.9);
  height: 0.875rem;
  line-height: 0.875rem;
  margin-top: 0.3125rem;
  margin-bottom: 0.3125rem;
  background-color: transparent;
  font-size: 0.75rem;
  cursor: pointer;
`;

const CartWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;
  padding-bottom: 5px;
  margin: 0 10px;
  box-sizing: border-box;
`;

const CartTarget = styled.div`
  position: relative;
`;

const CartButton = styled.div``;

const CartDrawContainer = styled.div`
  padding: 10px 0;
`;

const CartLink = styled.a`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin: 0 0 0 5px;
  position: relative;
  overflow: visible;
  outline: 0;
  text-decoration: none;
  background-color: transparent;
`;

const CartImg = styled.a`
  fill: currentColor;
  width: 26px;
  height: 26px;
  cursor: pointer;
  color: #fff;
  stroke: #fff;
  overflow: hidden;
  display: inline-block;
  position: relative;
  font-size: 1.0625rem;
  margin-right: 0.625rem;
`;
//========== Xử lý chỗ download =============

const HeaderQr = styled.div`
  width: 186px;
  background-color: #fff;
  position: absolute;
  left: 0;
  top: 118%;
  border-radius: 2px;
  display: none;

  &::before {
    position: absolute;
    left: 0;
    top: -16px;
    width: 100%;
    height: 20px;
    content: "";
    display: block;
  }
`;

const QrLink = styled.a`
  border: 0;
  color: #fff;
  text-decoration: none;
  font-size: 0.8125rem;
  font-weight: 450;
  padding: 0.25rem;
  position: relative;
  overflow: visible;
  outline: 0;
  background-color: transparent;

  &:hover {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
  }

  &:hover ${HeaderQr} {
    color: hsla(0, 0%, 100%, 0.7);
    cursor: pointer;
    display: block;
  }

  &::after {
    content: "";
    height: 0.9375rem;
    width: 0;
    border-left: 1px solid hsla(0, 0%, 100%, 0.22);
    border-right: 1px solid hsla(0, 0%, 100%, 0.22);
    position: absolute;
    left: -6px;
    top: calc(50% - 7px);
  }
`;

const HeaderQrImg = styled.img`
  width: 97%;
  padding: 1px;
`;

const HeaderApps = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const HeaderDownloadImg = styled.img`
  height: 16px;
`;

const Header = () => {
  return (
    <ShopeeTop>
      <NavbarWrapper>
        <ContainerNavbar>
          <LeftNavbar>
            <LeftNavbarLinkLeft href="https://banhang.shopee.vn/">
              Kênh Người Bán
            </LeftNavbarLinkLeft>
            <LeftNavbarLinkRight href="https://shopee.vn/m/sell-on-shopee">
              Trở thành Người bán Shopee
            </LeftNavbarLinkRight>
            <NavbarQr>
              <QrLink>
                Tải ứng dụng
                <HeaderQr>
                  <HeaderQrImg src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/d91264e165ed6facc6178994d5afae79.png"></HeaderQrImg>
                  <HeaderApps>
                    <HeaderDownloadImg
                      style={{ marginLeft: "11px" }}
                      src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/39f189e19764dab688d3850742f13718.png"
                    ></HeaderDownloadImg>
                    <HeaderDownloadImg
                      style={{ marginRight: "11px" }}
                      src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/f4f5426ce757aea491dce94201560583.png"
                    ></HeaderDownloadImg>
                    <HeaderDownloadImg
                      style={{ margin: "8px 0 0 11px", height: "13px" }}
                      src="https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg//assets/1ae215920a31f2fc75b00d4ee9ae8551.png"
                    ></HeaderDownloadImg>
                  </HeaderApps>
                </HeaderQr>
              </QrLink>
            </NavbarQr>
            <ConnectNavbar>Kết nối</ConnectNavbar>
            <SocialNavbar>
              <FacebookLink href="https://facebook.com/ShopeeVN"></FacebookLink>
              <InstagramLink href="https://instagram.com/Shopee_VN"></InstagramLink>
            </SocialNavbar>
          </LeftNavbar>

          <CenterNavbar></CenterNavbar>

          <RightNavbar>
            <NotificationNavbar>
              <NotiWrapper>
                <NotiButton>
                  <NotiLink>
                    <NotificationsNone
                      style={{
                        overflow: "hidden",
                        width: "1.15rem",
                        height: "1.15rem",
                        fill: "currentColor",
                        position: "relative",
                        display: "inline-block",
                        pointerEvents: "none",
                        color: "currentColor",
                        cursor: "pointer",
                        userSelect: "none",
                        listStyle: "none",
                      }}
                    />
                    <NotiTitle>Thông báo</NotiTitle>
                  </NotiLink>
                </NotiButton>
              </NotiWrapper>
            </NotificationNavbar>

            <SupportNavbar href="https://help.shopee.vn/vn/s">
              <SupportIcon>
                <HelpOutline
                  style={{
                    overflow: "hidden",
                    width: "1.15rem",
                    height: "1.15rem",
                    fill: "currentColor",
                    position: "relative",
                    display: "inline-block",
                    pointerEvents: "none",
                    color: "currentColor",
                    cursor: "pointer",
                    userSelect: "none",
                    listStyle: "none",
                  }}
                />
              </SupportIcon>
              <SupportTitle>Hỗ Trợ</SupportTitle>
            </SupportNavbar>

            <LanguageNavbar>
              <LanguageWrapper>
                <LanguageButton>
                  <LanguageItems>
                    <Language
                      style={{
                        overflow: "hidden",
                        width: "1.15rem",
                        height: "1.15rem",
                        fill: "currentColor",
                        position: "relative",
                        display: "inline-block",
                        pointerEvents: "none",
                        color: "currentColor",
                        cursor: "pointer",
                        userSelect: "none",
                        listStyle: "none",
                      }}
                    />
                    <LanguageTitle>Tiếng Việt</LanguageTitle>
                    <KeyboardArrowDown
                      style={{
                        overflow: "hidden",
                        width: "1.25rem",
                        height: "1.25rem",
                        fill: "currentColor",
                        position: "relative",
                        display: "inline-block",
                        pointerEvents: "none",
                        color: "currentColor",
                        cursor: "pointer",
                        userSelect: "none",
                        listStyle: "none",
                      }}
                    />
                  </LanguageItems>
                </LanguageButton>
              </LanguageWrapper>
            </LanguageNavbar>

            <RegisterNavbar>Đăng Ký</RegisterNavbar>
            <SeparatorNavbar></SeparatorNavbar>
            <LoginNavbar>Đăng Nhập</LoginNavbar>
          </RightNavbar>
        </ContainerNavbar>
      </NavbarWrapper>

      <ContainerWrapper>
        <ContainerWithSearh>
          <HeaderLogo href="/">
            <LogoWrapper>
              <LogoImg src="https://scontent.fsgn8-2.fna.fbcdn.net/v/t1.15752-9/292970690_1049701699247411_6270342906332781415_n.png?_nc_cat=100&ccb=1-7&_nc_sid=ae9488&_nc_ohc=0-6PJIAnYvQAX_vDmuj&_nc_ht=scontent.fsgn8-2.fna&oh=03_AVJA_H70IbxDAASTS7Uqe2soYQKgoemsrgmlaUQe5YwlCA&oe=6301787A"></LogoImg>
            </LogoWrapper>
          </HeaderLogo>

          <SearchSection>
            <SearchBar>
              <SearchBarMain>
                <SearchInputForm>
                  <SearchInput placeholder="Samsung - Giảm đến 50%"></SearchInput>
                </SearchInputForm>
              </SearchBarMain>
              <SearchButton>
                <Search />
              </SearchButton>
            </SearchBar>
            <SearchBottom>
              <SearchBottomItems>
                <SearchItemLink
                  href="https://shopee.vn/search?keyword=dép"
                  style={{ marginLeft: "0px" }}
                >
                  Dép
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=áo%20phông">
                  Áo Phông
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=váy">
                  Váy
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=dép%20nữ">
                  Dép Nữ
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=áo%20croptop">
                  Áo Croptop
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=túi%20xách%20nữ">
                  Túi Xách Nữ
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=ba%20lô">
                  Ba Lô
                </SearchItemLink>
                <SearchItemLink href="https://shopee.vn/search?keyword=áo%20khoác">
                  Áo Khoác
                </SearchItemLink>
              </SearchBottomItems>
            </SearchBottom>
          </SearchSection>

          <CartWrapper>
            <CartTarget>
              <CartButton>
                <CartDrawContainer>
                  <CartLink>
                    <CartImg>
                      <ShoppingCartOutlined />
                    </CartImg>
                  </CartLink>
                </CartDrawContainer>
              </CartButton>
            </CartTarget>
          </CartWrapper>
        </ContainerWithSearh>
      </ContainerWrapper>
    </ShopeeTop>
  );
};

export default Header;
