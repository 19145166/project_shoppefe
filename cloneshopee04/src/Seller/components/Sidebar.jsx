import React from "react";
import styled from "styled-components";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";

const Container = styled.div`
  position: fixed;
  z-index: 10;
  width: 220px;
  height: calc(100% - 56px);
  overflow: scroll;
  background: #fff;
  padding-top: 0px;
`;
const Sidebar = styled.div`
  width: inherit;
  left: 0;
  background-color: #fff;
`;
const Sidebar_menu = styled.ul`
  padding: 16px 16px 0;
  background-color: #fff;
  list-style: none;
`;
const Sidebar_menu_box = styled.li`
  margin-bottom: 24px;
  display: list-item;
  text-align: -webkit-match-parent;
`;
const Sidebar_menu_item = styled.div`
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
  flex-direction: row;
  -webkit-box-align: start;
  align-items: flex-start;
  color: #999;
  cursor: pointer;
  padding: 5px 0;
`;
const Sidebar_menu_item_icon = styled.img`
  color: #a2a2a4;
  width: 16px;
  height: 16px;
  margin-right: 8px;
`;
const Sidebar_menu_item_text = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 18px;
  max-width: 140px;
  font-weight: 600;
`;
const Sidebar_menu_item_space = styled.span`
  -webkit-box-flex: 1;
  flex: 1 0 8px;
`;
const Sidebar_submenu = styled.ul`
  padding-left: 24px;
  overflow: hidden;
  -webkit-transition: height 0.2s ease;
  transition: height 0.2s ease;
  list-style: none;
`;
const Sidebar_submenu_item = styled.li`
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
  flex-direction: row;
  -webkit-box-align: center;
  align-items: center;
  color: #333;
  font-size: 13px;
  line-height: 18px;
  padding: 5px 0;
`;
const Sidebar_submenu_item_link = styled.a`
  color: inherit;
  width: 100%;
  text-decoration: none;
  vertical-align: baseline;
  &:hover {
    color: #e84f20;
  }
`;

const SellerSidebar = () => {
  return (
    <Container>
      <Sidebar>
        <Sidebar_menu>
          {/* Vận chuyển */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/c15905d5a6284687c4a6ad00d0feb511"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Vận chuyển</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Quản Lý Vận Chuyển
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Giao Hàng Loạt
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Cài Đặt Vận Chuyển
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Quản lý đơn hàng */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/f82f8ccb649afcdf4f07f1dd9c41bcb0"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Quản lý đơn hàng</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>Tất Cả</Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>Đơn Hủy</Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Trả Hàng/Hoàn Tiền
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Quản lý sản phẩm */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/3fa3bdb20eb201ae3f157ee8d11a39d5"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Quản lý sản phẩm</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Tất Cả Sản Phẩm
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Thêm Sản Phẩm
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Sản Phẩm Vi Phạm
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Kênh marketing */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/2f9d62dd7e037c22608ac75dfb84a409"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Kênh Marketing</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Kênh Marketing
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Quảng Cáo Shopee
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Mã Giảm Giá Của Tôi
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Tài chính */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/f9e8756bcf783fe1783bf59577fdb263"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Tài chính</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>Doanh Thu</Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Số Dư TK Shopee
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Tài Khoản Ngân Hàng
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Thiết Lập Thanh Toán
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Dữ liệu */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/09759afab8ae066ca5e1630bc19133a1"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Dữ liệu</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Phân Tích Bán Hàng
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Hiệu Quản Hoạt Động
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Phát triển */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/ed031c6a398cc8b319b4ab17bd071843"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Phát triển</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Nhiệm Vụ Người Bán
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Shop Yêu Thích
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Chăm sóc khách hàng */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/9f2ae273250a1a723d7d8892c9584c6d"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>
                Chăm sóc khách hàng
              </Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Trợ Lý Chat
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>Hỏi - Đáp</Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Quản lý shop */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/6b1ffcde1ff12621088110f419a5283a"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Quản lý Shop</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Đánh Giá Shop
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Hồ Sơ Shop
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Trang Trí Shop
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Danh Mục Của Shop
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Kho Hình Ảnh/Video
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Báo Cáo Của Tôi
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
          {/* Thiết lập shop */}
          <Sidebar_menu_box>
            <Sidebar_menu_item>
              <Sidebar_menu_item_icon src="https://cf.shopee.vn/file/789f116a0778cf137519cadb1165d70f"></Sidebar_menu_item_icon>
              <Sidebar_menu_item_text>Thiết lập Shop</Sidebar_menu_item_text>
              <Sidebar_menu_item_space></Sidebar_menu_item_space>
              <ExpandLessIcon></ExpandLessIcon>
            </Sidebar_menu_item>
            <Sidebar_submenu>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>Địa Chỉ</Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Thiết Lập Shop
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>Tài Khoản</Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
              <Sidebar_submenu_item>
                <Sidebar_submenu_item_link>
                  Nền tảng đối tác(Kết nối API)
                </Sidebar_submenu_item_link>
              </Sidebar_submenu_item>
            </Sidebar_submenu>
          </Sidebar_menu_box>
        </Sidebar_menu>
      </Sidebar>
    </Container>
  );
};
export default SellerSidebar;
