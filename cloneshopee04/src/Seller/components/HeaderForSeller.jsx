import React from "react";
import styled from "styled-components";
import AppsIcon from "@mui/icons-material/Apps";
import GridViewIcon from "@mui/icons-material/GridView";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import HeaderNotify from "./Header_notify";

const Header__bar = styled.div`
  top: 0;
  left: 0;
  position: fixed;
  width: 100%;
  min-height: 56px;
  max-height: 56px;
  z-index: 6666;
  background-color: #fff;
  -webkit-box-shadow: 0 1px 4px 0 rgb(74 74 78 / 12%);
  box-shadow: 0 1px 4px 0 rgb(74 74 78 / 12%);
`;
const Header__content = styled.div`
  display: flex;
  width: 100vw;
  margin: auto;
  min-height: 56px;
  max-height: 56px;
  align-items: center;
  position: relative;
`;
const LogoImg = styled.img`
  width: 97px;
  height: 31px;
  margin-left: 16px;
  cursor: pointer;
`;
const Path = styled.div`
  color: #1d1d1d;
  font-size: 18px;
  -webkit-tap-highlight-color: transparent;
  cursor: pointer;
  margin-left: 5px;
`;
const ShopeeUni = styled.a`
  padding: -1 16px;
  border: 1px solid #dcdce0;
  border-radius: 16px;
  line-height: 34px;
  text-align: center;
  color: #333;
  font-size: 12px;
  text-decoration: none;
  width: 75px;
  height: 32px;
  -webkit-tap-highlight-color: transparent;
  margin: 0 5px;

  &:hover {
    background-color: #fafafa;
    cursor: pointer;
  }
`;
const Account = styled.a`
  align-items: center;
  display: flex;
  border-radius: 16px;
  line-height: 40px;
  padding: 0 2px;
  color: #333;
  min-width: 120px;
  height: 36px;

  &:hover {
    background-color: #e5e5e5;
    cursor: pointer;
  }
`;
const Avatar = styled.img`
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding-right: 5px;
`;
const Span = styled.span`
  height: 34px;
  width: 1.5px;
  background-color: #e5e5e5;
  margin: 0 20px;
`;
const LeftNavbar = styled.div`
  display: flex;
  align-items: center;
`;
const RightNavbar = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  padding: 5px;
  margin: 15px;
`;
const CenterNavbar = styled.div`
  flex: 1;
  color: #fff;
`;
const GridApp = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  align-items: center;
  line-height: 54px;
  text-align: center;
  &:hover {
    background-color: #e5e5e5;
    cursor: pointer;
  }
`;
const Notify = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  align-items: center;
  line-height: 54px;
  text-align: center;
  margin: 0 5px;
  position: relative;
  &:hover {
    background-color: #e5e5e5;
    cursor: pointer;
  }
`;

const Header = () => {
  return (
    <Header__bar>
      <Header__content>
        <LeftNavbar>
          <a>
            <LogoImg src="https://i.pinimg.com/originals/a0/f1/8d/a0f18d53895806b098258ed4cefb920d.png"></LogoImg>
          </a>
          <Path>Kênh Người Bán</Path>
        </LeftNavbar>
        <CenterNavbar></CenterNavbar>
        <RightNavbar>
          <Account>
            <Avatar src="https://scontent.fsgn5-6.fna.fbcdn.net/v/t1.6435-9/39930058_685302941817834_2648646549793406976_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=174925&_nc_ohc=DE77sB6OS1cAX8TkJP_&_nc_ht=scontent.fsgn5-6.fna&oh=00_AT8aGnz8xjaSCFAevqSvIafXMWDISYoiyQ_Mo3uy_BoWgA&oe=63089265"></Avatar>
            HoangDuy
          </Account>
          <Span></Span>
          <GridApp>
            <GridViewIcon />
          </GridApp>
          <Notify>
            <NotificationsNoneIcon />
          </Notify>
          <ShopeeUni>SHOPEE UNI</ShopeeUni>
        </RightNavbar>
      </Header__content>
    </Header__bar>
  );
};
export default Header;
