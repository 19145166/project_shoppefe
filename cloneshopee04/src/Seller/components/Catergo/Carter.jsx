import "./Carter.css";

export default function Carter() {
  return (
    <div className="chart">
      <h3 className="chartTitle">Danh sách cần làm</h3>
      <p className="title-tip">Những việc bạn sẽ phải làm</p>

      <div data-v-3a7c6ce3="" data-v-066e19c8="" class="to-do-box">
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Chờ Xác Nhận
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Chờ Lấy Hàng
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Đã Xử Lý
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Đơn Hủy
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Trả Hàng/Hoàn Tiền Chờ Xử Lý
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Sản Phẩm Bị Tạm Khóa
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Sản Phẩm Hết Hàng
          </p>
        </a>
        <a
          data-v-3a7c6ce3=""
          href="/#"
          class="to-do-box-aitem"
          data-v-066e19c8=""
        >
          <p data-v-3a7c6ce3="" class="item-title">
            0
          </p>{" "}
          <p data-v-3a7c6ce3="" class="item-desc">
            Chương Trình Khuyến Mãi Chờ Xử Lý
          </p>
        </a>
      </div>
    </div>
  );
}
