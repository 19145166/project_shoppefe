import "./Notify.css";

export default function Notify() {
  return (
    <div data-v-1a3edc0c="" class="aside-column">
      <div data-v-36161b20="" data-v-1a3edc0c="" class="card card-offset">
        <div data-v-36161b20="" class="title-box">
          <div data-v-36161b20="" class="title">
            Thông Báo
          </div>
          <button
            data-v-36161b20=""
            type="button"
            class="shopee-button shopee-button--link"
          >
            <span>Xem thêm</span>
            <i class="shopee-icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                <path d="M9.19 8l-3.97 3.97a.75.75 0 0 0 1.06 1.06l4.5-4.5a.75.75 0 0 0 0-1.06l-4.5-4.5a.75.75 0 0 0-1.06 1.06L9.19 8z"></path>
              </svg>
            </i>
          </button>
        </div>
        <div
          data-v-066e19c8=""
          data-v-36161b20=""
          class="async-data-wrapper announcement-wrapper"
        >
          <div data-v-066e19c8="" class="status">
            <div data-v-36161b20="" data-v-066e19c8="" class="box">
              <div data-v-36161b20="" data-v-066e19c8="" class="content">
                <div
                  data-v-36161b20=""
                  data-v-066e19c8=""
                  class="item item-hover"
                >
                  <p data-v-36161b20="" data-v-066e19c8="" class="title">
                    ĐĂNG KÝ NHẬN HIỂN THỊ SALE 8.8
                  </p>
                  <p data-v-36161b20="" data-v-066e19c8="" class="desc">
                    🙋Nhanh tay tạo [Combo khuyến mãi] để xuất hiện trên trang
                    Khuyến Mãi của Shopee SIÊU SALE 7.7: ✅Giảm từ 5% hoặc
                    10.000 VNĐ ✅Mua không quá 10 sản phẩm để được giảm giá
                    ✅Thời hạn Deal: ít nhất từ 3/08 - 12/08 Tham gia TẠI ĐÂY
                  </p>
                </div>
                <span data-v-36161b20="" data-v-066e19c8="" class="new-icon">
                  <span data-v-36161b20="" data-v-066e19c8="">
                    Mới
                  </span>{" "}
                  ·{" "}
                </span>
                <span data-v-36161b20="" data-v-066e19c8="" class="mtime">
                  Hôm Nay 00:00
                </span>
              </div>
            </div>

            <div data-v-36161b20="" data-v-066e19c8="" class="box">
              <div data-v-36161b20="" data-v-066e19c8="" class="content">
                <div
                  data-v-36161b20=""
                  data-v-066e19c8=""
                  class="item item-hover"
                >
                  <p data-v-36161b20="" data-v-066e19c8="" class="title">
                    8.8 SHOP ƠI CHÚ Ý
                  </p>
                  <p data-v-36161b20="" data-v-066e19c8="" class="desc">
                    Shop đã chuẩn bị bí kíp đẩy đơn trong chiến dich 8.8 SIÊU
                    SALE GIẢM 50% chưa? 👉 Nhấn để XEM NGAY!
                  </p>
                </div>
                <span data-v-36161b20="" data-v-066e19c8="" class="new-icon">
                  <span data-v-36161b20="" data-v-066e19c8="">
                    Mới
                  </span>{" "}
                  ·{" "}
                </span>
                <span data-v-36161b20="" data-v-066e19c8="" class="mtime">
                  Hôm Nay 00:00
                </span>
              </div>
            </div>

            <div data-v-36161b20="" data-v-066e19c8="" class="box">
              <div data-v-36161b20="" data-v-066e19c8="" class="content">
                <div
                  data-v-36161b20=""
                  data-v-066e19c8=""
                  class="item item-hover"
                >
                  <p data-v-36161b20="" data-v-066e19c8="" class="title">
                    ĐĂNG KÝ NHẬN HIỂN THỊ SALE 8.8
                  </p>
                  <p data-v-36161b20="" data-v-066e19c8="" class="desc">
                    🙋Nhanh tay tạo [Combo khuyến mãi] để xuất hiện trên trang
                    Khuyến Mãi của Shopee SIÊU SALE 7.7: ✅Giảm từ 5% hoặc
                    10.000 VNĐ ✅Mua không quá 10 sản phẩm để được giảm giá
                    ✅Thời hạn Deal: ít nhất từ 3/08 - 12/08 Tham gia TẠI ĐÂY
                  </p>
                </div>
                <span data-v-36161b20="" data-v-066e19c8="" class="new-icon">
                  <span data-v-36161b20="" data-v-066e19c8="">
                    Mới
                  </span>{" "}
                  ·{" "}
                </span>
                <span data-v-36161b20="" data-v-066e19c8="" class="mtime">
                  28 Tháng 7 2022
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
