import React from "react";
import styled from "styled-components";

const Container = styled.div`
    position: absolute;
    z-index: 1;
    top: calc(100% + 14px);
    right: 0;
    width: 404px;
    background-color: #ffff;
    border: 1px solid #D3D3D3;
    border-radius: 2px;    
    animation: headerNotifyGrowth ease-in 0.2s;
    transform-origin: calc(100%-32px) top;
    will-change: opacity, transform;
    text-align: left;
    display: none;
`;
const Header__notify_header = styled.header`
    height: 40px;
    border-bottom: 1px solid  rgba(0,0,0,.09);
`;
const Header__notify_header_content = styled.h3`
    color: #999;
    font-weight: 400;
    font-size: 0.875rem;
    line-height: 40px;
    margin: 0 0 0 10px;
    user-select: none;
    &:hover{
        cursor: pointer;
    }
`;
const Header__notify_list = styled.ul`
    list-style: none;
    padding-left:0;
    display:  block;
`;

const Header__notify_item = styled.li`
    display: flex;
    margin: 5px 0;
    &:hover{
        background-color: #e7e5e5;
    }
`;
const Header__notify_link = styled.a`
    text-decoration: none;
    display: flex;
    padding: 12px;
`;
const Header__notify_img = styled.img`
    width: 48px;
    object-fit: contain;
`;
const Header__notify_infor = styled.div`
    margin-left: 12px;
`;
const Header__notify_name = styled.span`
    font-size: 0.875rem;
    display: block;
    color: var(--text-color);
    font-weight: 400;
    line-height: 1.4;
`;
const Header__notify_description = styled.span`
    font-size: 0.875rem;
    color: #bbb;
    display: block;
    line-height: 1.4;
`;
const Header__notify_footer = styled.footer`
    &:hover {
        background-color: #e7e5e5;
    }
`;
const Header__notify_btn = styled.a`
    text-decoration: none;
    color: #05a;
    font-size: 0.875rem;
    text-align: center;
    width: 100%;
    display: block;
`;

const HeaderNotify =()=>{
    return(
        
        <Container>
             <Header__notify_header>
                <Header__notify_header_content>Thông Báo Mới Nhận</Header__notify_header_content>
            </Header__notify_header>
            <Header__notify_list>
                <Header__notify_item>
                    <Header__notify_link></Header__notify_link>
                    <span>
                        <Header__notify_img src="https://cf.shopee.vn/file/b8d975bdd58d70fd64ded9bc954e6f5c"></Header__notify_img>
                    </span>
                    <Header__notify_infor>
                        <Header__notify_name>Voucher Freeship</Header__notify_name>
                        <Header__notify_description>Mã giảm giá lên đến 15k cho đơn từ 99k. Mã giảm giá đến
                          50k cho đơn từ 250k</Header__notify_description>
                    </Header__notify_infor>
                </Header__notify_item>
                <Header__notify_item>
                    <Header__notify_link></Header__notify_link>
                    <span>
                        <Header__notify_img src="https://cf.shopee.vn/file/b8d975bdd58d70fd64ded9bc954e6f5c"></Header__notify_img>
                    </span>
                    <Header__notify_infor>
                        <Header__notify_name>Voucher Freeship</Header__notify_name>
                        <Header__notify_description>Mã giảm giá lên đến 15k cho đơn từ 99k. Mã giảm giá đến
                          50k cho đơn từ 250k</Header__notify_description>
                    </Header__notify_infor>
                </Header__notify_item>
                <Header__notify_item>
                    <Header__notify_link></Header__notify_link>
                    <span>
                        <Header__notify_img src="https://cf.shopee.vn/file/b8d975bdd58d70fd64ded9bc954e6f5c"></Header__notify_img>
                    </span>
                    <Header__notify_infor>
                        <Header__notify_name>Voucher Freeship</Header__notify_name>
                        <Header__notify_description>Mã giảm giá lên đến 15k cho đơn từ 99k. Mã giảm giá đến
                          50k cho đơn từ 250k</Header__notify_description>
                    </Header__notify_infor>
                </Header__notify_item>
                <Header__notify_item>
                    <Header__notify_link></Header__notify_link>
                    <span>
                        <Header__notify_img src="https://cf.shopee.vn/file/b8d975bdd58d70fd64ded9bc954e6f5c"></Header__notify_img>
                    </span>
                    <Header__notify_infor>
                        <Header__notify_name>Voucher Freeship</Header__notify_name>
                        <Header__notify_description>Mã giảm giá lên đến 15k cho đơn từ 99k. Mã giảm giá đến
                          50k cho đơn từ 250k</Header__notify_description>
                    </Header__notify_infor>
                </Header__notify_item>
            </Header__notify_list>
            <Header__notify_footer>
                <Header__notify_btn>Xem tất cả thông báo</Header__notify_btn>
            </Header__notify_footer>
        </Container>
    );
};
export default HeaderNotify;