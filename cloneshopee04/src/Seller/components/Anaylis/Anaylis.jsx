import "./Anaylis.css";

export default function Anaylis() {
  return (
    <div>
      <div data-v-5d186296="" data-v-1a3edc0c="" class="card card-offset">
        <div data-v-5d186296="" class="title-box">
          <div data-v-5d186296="" class="title">
            Phân Tích Bán Hàng
            <span data-v-5d186296="" class="title-tip">
              ( Hôm nay 00:00 GMT+7 09:00 )
            </span>{" "}
            <p data-v-5d186296="" class="card-tips">
              Tổng quan về dữ liệu của cửa hàng cho kích thước của đơn hàng đã
              xác nhận
            </p>
          </div>{" "}
          <button
            data-v-5d186296=""
            type="button"
            class="shopee-button shopee-button--link shopee-button--normal"
          >
            <span>Xem thêm</span>
            <i class="shopee-icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
                <path d="M9.19 8l-3.97 3.97a.75.75 0 0 0 1.06 1.06l4.5-4.5a.75.75 0 0 0 0-1.06l-4.5-4.5a.75.75 0 0 0-1.06 1.06L9.19 8z"></path>
              </svg>
            </i>
          </button>
        </div>{" "}
        <div data-v-066e19c8="" data-v-5d186296="" class="async-data-wrapper">
          <div data-v-066e19c8="" class="status">
            <div data-v-5d186296="" data-v-066e19c8="" class="content">
              <div data-v-5d186296="" data-v-066e19c8="" class="list-box">
                <div data-v-5d186296="" data-v-066e19c8="" class="item">
                  <div
                    data-v-5d186296=""
                    data-v-066e19c8=""
                    class="item-box text-overflow"
                  >
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-title text-overflow"
                    >
                      Lượt truy cập
                      <div
                        data-v-5d186296=""
                        class="shopee-popover shopee-popover--light"
                        data-v-066e19c8=""
                      ></div>
                    </p>{" "}
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-number text-overflow"
                    >
                      0
                    </p>{" "}
                    <div
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-increase text-overflow"
                    >
                      <p
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="text-overflow"
                      >
                        Vs hôm qua
                      </p>{" "}
                      <div
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="item-increase-number"
                      >
                        <p
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="text-overflow"
                        >
                          0,00%
                        </p>{" "}
                        <i
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="item-increase-number-icon"
                        >
                          <svg>
                            <rect
                              x="3"
                              y="7"
                              width="9"
                              height="2.029"
                              rx=".5"
                            ></rect>
                          </svg>
                        </i>
                      </div>
                    </div>
                  </div>{" "}
                  <div
                    data-v-5d186296=""
                    data-v-066e19c8=""
                    class="item-box text-overflow"
                  >
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-title text-overflow"
                    >
                      Lượt xem
                      <div
                        data-v-5d186296=""
                        class="shopee-popover shopee-popover--light"
                        data-v-066e19c8=""
                      ></div>
                    </p>{" "}
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-number text-overflow"
                    >
                      0
                    </p>{" "}
                    <div
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-increase text-overflow"
                    >
                      <p
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="text-overflow"
                      >
                        Vs hôm qua
                      </p>{" "}
                      <div
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="item-increase-number"
                      >
                        <p
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="text-overflow"
                        >
                          0,00%
                        </p>{" "}
                        <i
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="item-increase-number-icon"
                        >
                          <svg>
                            <rect
                              x="3"
                              y="7"
                              width="9"
                              height="2.029"
                              rx=".5"
                            ></rect>
                          </svg>
                        </i>
                      </div>
                    </div>
                  </div>
                </div>{" "}
                <div data-v-5d186296="" data-v-066e19c8="" class="item">
                  <div
                    data-v-5d186296=""
                    data-v-066e19c8=""
                    class="item-box text-overflow"
                  >
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-title text-overflow"
                    >
                      Đơn hàng
                      <div
                        data-v-5d186296=""
                        class="shopee-popover shopee-popover--light"
                        data-v-066e19c8=""
                      ></div>
                    </p>{" "}
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-number text-overflow"
                    >
                      0
                    </p>{" "}
                    <div
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-increase text-overflow"
                    >
                      <p
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="text-overflow"
                      >
                        Vs hôm qua
                      </p>{" "}
                      <div
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="item-increase-number"
                      >
                        <p
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="text-overflow"
                        >
                          0,00%
                        </p>{" "}
                        <i
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="item-increase-number-icon"
                        >
                          <svg>
                            <rect
                              x="3"
                              y="7"
                              width="9"
                              height="2.029"
                              rx=".5"
                            ></rect>
                          </svg>
                        </i>
                      </div>
                    </div>
                  </div>{" "}
                  <div
                    data-v-5d186296=""
                    data-v-066e19c8=""
                    class="item-box text-overflow"
                  >
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-title text-overflow"
                    >
                      Tỷ lệ chuyển đổi
                      <div
                        data-v-5d186296=""
                        class="shopee-popover shopee-popover--light"
                        data-v-066e19c8=""
                      ></div>
                    </p>{" "}
                    <p
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-number text-overflow"
                    >
                      0,00%
                    </p>{" "}
                    <div
                      data-v-5d186296=""
                      data-v-066e19c8=""
                      class="item-increase text-overflow"
                    >
                      <p
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="text-overflow"
                      >
                        Vs hôm qua
                      </p>{" "}
                      <div
                        data-v-5d186296=""
                        data-v-066e19c8=""
                        class="item-increase-number"
                      >
                        <p
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="text-overflow"
                        >
                          0,00%
                        </p>{" "}
                        <i
                          data-v-5d186296=""
                          data-v-066e19c8=""
                          class="item-increase-number-icon"
                        >
                          <svg>
                            <rect
                              x="3"
                              y="7"
                              width="9"
                              height="2.029"
                              rx=".5"
                            ></rect>
                          </svg>
                        </i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
