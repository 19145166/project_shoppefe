import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Header from "../Seller/components/HeaderForSeller";
import Siderbar from "../Seller/components/Sidebar";
import Slide from "../Seller/components/Slide/Slide";
import Cater from "../Seller/components/Catergo/Carter";
import Anylis from "../Seller/components/Anaylis/Anaylis";
import Notify from "../Seller/components/Notify/Notify";
const Home = styled.div`
  flex: 4;
`;
const HomeWidgets = styled.div`
  margin-left: 340px;
  margin-top: 65px;
`;
Seller.propTypes = {};

function Seller(props) {
  return (
    <div>
      <Header />
      <Siderbar />
      <Home>
        <HomeWidgets>
          <Slide />
          <Cater />
          <br />
          <Anylis />
          <Notify />
        </HomeWidgets>
      </Home>
    </div>
  );
}
export default Seller;
