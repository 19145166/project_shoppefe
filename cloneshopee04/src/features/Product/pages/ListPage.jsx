import React, { useEffect, useMemo, useState } from "react";
import PropTypes from "prop-types";

import { Grid, Container, Box, makeStyles } from "@material-ui/core";
import { Pagination, Paper, Typography } from "@mui/material";

import queryString from "query-string";
import productAPI from "../../../api/productAPI";
import ProductSkeletonList from "../components/ProductSkeletonList";
import ProductList from "../components/ProductList";
import ProductSort from "../components/ProductSort";
import ProductFilters from "../components/ProductFilters";
import { useHistory, useLocation } from "react-router-dom";

ListPage.propTypes = {};
const useStyles = makeStyles((theme) => ({
  root: {},
  left: {
    width: "250px",
  },
  right: {
    flex: "1 1 0",
  },
  pagination: {
    display: "flex",
    flexFlow: "row nowrap",
    justifyContent: "center",

    marginTop: "20px",
    paddingBottom: "20px",
  },
}));
function ListPage(props) {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation(); //Nếu url thay đổi thì liên tục trả về môt object mới
  const queryParams = useMemo(() => {
    const params = queryString.parse(location.search);
    // {isPromotion: "true"}

    return {
      ...params,
      _page: Number.parseInt(params._page) || 1,
      _limit: Number.parseInt(params._limit) || 8,
      _sort: params._sort || "salePrice:ASC",
    };
  }, [location.search]);
  const [productList, setProductList] = useState([]);
  const [Loading, setLoading] = useState(true);
  const [pagination, setPagination] = useState({
    limit: 8,
    total: 5,
    page: 1,
  });
  // const [filters, setFilters] = useState({
  //   _page: 1,
  //   _limit: 8,
  //   _sort: "salePrice:ASC",
  // });

  //
  // const [filters, setFilters] = useState(() => ({
  //   ...queryParams,
  //   _page: Number.parseInt(queryParams._page) || 1,
  //   _limit: Number.parseInt(queryParams._limit) || 8,
  //   _sort: queryParams._sort || "salePrice:ASC",
  // }));

  //
  // useEffect(() => {
  //   history.push({
  //     pathname: history.location.pathname,
  //     search: queryString.stringify(filters),
  //   });
  // }, [history, filters]);
  useEffect(() => {
    (async () => {
      try {
        const { data, pagination } = await productAPI.getAll(queryParams);

        setProductList(data);
        setPagination(pagination);
      } catch (error) {
        console.log("Fail to fetch product list: ", error);
      }

      setLoading(false);
    })();
  }, [queryParams]); // Mỗi khi filters thay đổi thì get lại sản phẩm
  // Lấy dữ liệu filter mới
  const handlePageChange = (e, page) => {
    // setFilters((prevFilters) => ({
    //   ...prevFilters,
    //   _page: page,
    // }));
    const filters = {
      ...queryParams,
      _page: page,
    };

    history.push({
      pathname: history.location.pathname,
      search: queryString.stringify(filters),
    });
  };

  const handleSortChange = (newSortValue) => {
    // setFilters((prevFilters) => ({
    //   ...prevFilters,
    //   _sort: newSortValue,
    // }));

    const filters = {
      ...queryParams,
      _sort: newSortValue,
    };

    history.push({
      pathname: history.location.pathname,
      search: queryString.stringify(filters),
    });
  };
  const handleFiltersChange = (newFilters) => {
    // setFilters((prevFilters) => ({
    //   ...prevFilters,
    //   ...newFilters,
    // }));

    const filters = {
      ...queryParams,
      ...newFilters,
    };

    history.push({
      pathname: history.location.pathname,
      search: queryString.stringify(filters),
    });
  };
  return (
    <Box>
      <Container>
        <Grid container spacing={1}>
          <Grid item className={classes.left}>
            <Paper elevation={0}>
              <ProductFilters
                filters={queryParams}
                onChange={handleFiltersChange}
              />
            </Paper>
          </Grid>
          <Grid item className={classes.right}>
            <ProductSort
              currentSort={queryParams._sort}
              onChange={handleSortChange}
            />
            <Paper elevation={0}>
              {Loading ? (
                <ProductSkeletonList length={8} />
              ) : (
                <ProductList data={productList} />
              )}
              <Box className={classes.pagination}>
                <Pagination
                  shape="rounded"
                  color="warning"
                  count={Math.ceil(pagination.total / pagination.limit)}
                  page={pagination.page}
                  onChange={handlePageChange}
                ></Pagination>
              </Box>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
}

export default ListPage;
