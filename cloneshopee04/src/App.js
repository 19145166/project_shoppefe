import logo from "./logo.svg";
//import "./App.css";
import React, { useEffect } from "react";

import AlbumFeature from "./features/Album";
import { Route, Link, NavLink, Switch, Redirect } from "react-router-dom";
import NotFound from "./components/NotFound";
import CounterFeatures from "./features/Counter";
import Header from "./components/Header";
import ProductFeature from "./features/Product";
import Footer from "./components/Footer/Footer";
import PaymentPage from "./pages/Payment";
import Seller from "./Seller"

function App() {
  return (
    <div className="App">
       {/* <Header />  */}

      {/* Kiểm tra path xem có cái nào match đưuọc với path hiện tại hay không thì hiện ra nhưng chỉ một Route */}
      <Switch>
        {/* Điều hướng và ẩn link Url */}
        <Redirect from="/home" to="/"></Redirect>

        <Route path="/" component={CounterFeatures} exact />
        <Route path="/albums" component={AlbumFeature} />
        <Route path="/products" component={ProductFeature} />
        <Route path="/payments" component={PaymentPage} />
        <Route path="/sellers" component={Seller} />
        <Route component={NotFound} />
      </Switch>
      <br />
      {/* <Footer /> */}
    </div>
  );
}

export default App;
